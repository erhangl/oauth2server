# COMPOSER PAKETLERİNİ YÜKLEYİN
```
composer install
```

# DOCKER'I AYAĞA KALDIRIN
```
docker-compose build
docker-compose up -d
```

# HOST DOSYANIZI YAPILANDIRIN
```
127.0.0.1 oauth2server.com
```

# DOCKER CONTAINER TERMINALINE GEÇIN
```
docker exec -it oauth2-php-fpm bash
```

# VERITABANINI YAPILANDIRIN
```
php artisan migrate
php artisan db:seed
```
Test User'ı eklenmiştir. Username: test@test.com Password: 123456

# PASSPORT CLIENT'I YAPILANDIRIN
```
php artisan passport:install
```
Yukarıdaki komuttan sonra "Password grant client created successfully." mesajını göreceksiniz. Bunun altındaki Client ID ve Client secret parametrelerinizi kaydedin.

# PRIVATE ve PUBLIC KEY OLUŞTURUN
```
php artisan passport:keys
```
# TOKEN ALMAK (LOGIN)
```
$http = new GuzzleHttp\Client;

$response = $http->post('http://oauth2server.com:8080/oauth/token', [
    'form_params' => [
        'grant_type' => 'password',
        'client_id' => 'client-id',
        'client_secret' => 'client-secret',
        'username' => 'test@test.com',
        'password' => '123456',
        'scope' => '',
    ],
]);

return json_decode((string) $response->getBody(), true);
```

# TOKEN İLE USER BİLGİSİ ALMAK
```
$response = $client->request('GET', 'http://oauth2server.com:8080/api/user', [
    'headers' => [
        'Accept' => 'application/json',
        'Authorization' => 'Bearer '.$accessToken,
    ],
]);

return json_decode((string) $response->getBody(), true);
```

# HOST DOSYAMIZI GÜNCELLEYELİM
```
127.0.0.1 oauth2server.com
```
